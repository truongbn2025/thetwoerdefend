using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTurret : MonoBehaviour
{
    public GameObject turretPrefabs;
    public List<Transform> spawnPoint;
    private void Start()
    {
        foreach (Transform point in spawnPoint)
        {
            var turret = Instantiate(turretPrefabs, point.position, Quaternion.identity);
            MachineGun props = turret.transform.GetComponent<MachineGun>();
            props.damage += PlayerPrefs.GetInt("DamageUpgrade") * 1f;
            props.fireRange += PlayerPrefs.GetInt("FireRangeUpgrade") * 1f;
            float percentage = Mathf.Pow(0.95f, PlayerPrefs.GetInt("FireRateUpgrade"));
            Debug.Log(percentage);
            Debug.Log(PlayerPrefs.GetInt("FireRateUpgrade"));
            props.fireRate = props.fireRate * percentage;
        }
    }
}
