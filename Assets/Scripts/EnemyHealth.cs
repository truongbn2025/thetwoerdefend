using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float health = 50f;
    BoxCollider enemyHurtbox;
    private float maxHealth = 50f;
    public Slider healthBar;

    public delegate void EnemyDeadEvent();
    public static event EnemyDeadEvent OnEnemyDead;

    //private Animation deathAnimation;
    // Start is called before the first frame update

    private void Start()
    {
        enemyHurtbox = GetComponentInChildren<BoxCollider>();
        health = maxHealth;
    }


   

    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            GetComponent<MonsterNavmesh>().TransitionToState(ENEMY_STATE.DIE);
            enemyHurtbox.enabled = false;
            OnEnemyDead?.Invoke();
            int temp = UnityEngine.Random.Range(1, 100);
            if(temp < 20)
            {
                LevelManager.Instance.collectedCoin++;
            }
        }
        healthBar.value = (float)health / maxHealth;
    }
}
