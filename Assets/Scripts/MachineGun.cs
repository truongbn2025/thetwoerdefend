using System.Collections;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.InputSystem.LowLevel.InputStateHistory;
using static UnityEngine.ParticleSystem;
enum TURRET_STATE
{
    IDLE,
    FIRING,
    RELOADING,
    RELOCATING,
    UPGRADING
}
public class MachineGun : Turret
{
    private float nextFireTime;
    private bool isReloading;
    private TURRET_STATE turretState;

    public int index;
    public Slider ammoBar;
    private void Start()
    {
        base.Start();
        turretState = TURRET_STATE.IDLE;
        nextFireTime = Time.time + fireRate;   
    }
    private void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (other.CompareTag(enemyTag) || other.CompareTag(secondaryEnemyTag))
        {
            
        }
    }

    private void Update()
    {
        //Debug.Log(turretState);
        switch (turretState)
        {
            case TURRET_STATE.IDLE:
                if(target != null)
                {
                    if (clipSize <=0)
                    {
                        TransitionToState(TURRET_STATE.RELOADING);
                    }
                    else
                    {
                        TransitionToState(TURRET_STATE.FIRING);
                    }
                }
                break;
            case TURRET_STATE.RELOADING:
                if(target != null && Time.time >= nextFireTime) {
                    TransitionToState(TURRET_STATE.FIRING);
                }else if(target == null)
                {
                    TransitionToState(TURRET_STATE.IDLE);
                }
                break;
            case TURRET_STATE.FIRING:
                if(target == null)
                {
                    TransitionToState(TURRET_STATE.IDLE);
                }
                if(clipSize <= 0)
                {
                    TransitionToState(TURRET_STATE.RELOADING);
                }
                if (target != null)
                {

                    CalculateAimDirection(target.transform.position);
                    LookAtEnemy();
                    if (Time.time >= nextFireTime && !isReloading)
                    {
                        FireProjectile();
                    }
                }
                break;
            case TURRET_STATE.RELOCATING:
                break;
            case TURRET_STATE.UPGRADING:
                break;
        }
        ammoBar.value = ((float)clipSize / (float)maxAmmoClip);
        

    }

    #region Methods

    private void TransitionToState(TURRET_STATE newState)
    {
        // Perform any necessary exit logic for the current state
        switch (turretState)
        {
            case TURRET_STATE.IDLE:
                break;
            case TURRET_STATE.RELOADING:
                break;
            case TURRET_STATE.FIRING:

                break;
            case TURRET_STATE.RELOCATING:
                break;
            case TURRET_STATE.UPGRADING:
                break;
        }

        // Update the current state
        turretState = newState;

        // Perform any necessary entry logic for the new state
        switch (turretState)
        {
            case TURRET_STATE.IDLE:
                break;
            case TURRET_STATE.RELOADING:
                break;
            case TURRET_STATE.FIRING:

                break;
            case TURRET_STATE.RELOCATING:
                break;
            case TURRET_STATE.UPGRADING:
                break;
        }
    }

    private void FireProjectile()
    {
        if (clipSize <= 0)
        {
            
            Reload();
            return;
        }
        GameObject proj = Instantiate(projectile, muzzlePositions[0].position, muzzlePositions[0].rotation);
        proj.transform.position = muzzlePositions[0].position;
        proj.transform.rotation = muzzlePositions[0].rotation;
        
        Projectile prjScript = proj.GetComponent<Projectile>();
        
        prjScript.targetPosition = target.transform.position;
        prjScript.damage = damage;
        if (muzzleSpark)
        {
            GameObject muzzle = Instantiate(muzzleSpark, muzzlePositions[0].position, muzzlePositions[0].rotation); 
            if (muzzle == null) {
                return;
            }
            muzzle.transform.position = muzzlePositions[0].position;
            muzzle.transform.rotation = muzzlePositions[0].rotation;
            muzzle.SetActive(true);
        }
        //muzzle


        clipSize -= 1;
        if (clipSize <= 0)
        {
            Reload();
            return;
        }
        nextFireTime = Time.time + fireRate;
    }
    public void RelocateTurret()
    {
        TransitionToState(TURRET_STATE.RELOCATING);
    }
    public void IdleTurret()
    {
        TransitionToState(TURRET_STATE.IDLE);
    }
    public void UpgradeTurret()
    {
        TransitionToState(TURRET_STATE.UPGRADING);
    }
    void Reload()
    {
        nextFireTime = Time.time + reloadTime;
        clipSize = maxAmmoClip;
        isReloading = false;
        TransitionToState(TURRET_STATE.RELOADING);
    }
    #endregion

}
