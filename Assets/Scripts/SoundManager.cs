using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }
    public AudioSource music;
    public AudioSource clickSound;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        
        }
        else
        {
     
            Destroy(gameObject);
        }
    }

    public void ChangeMusicVolume(float volume)
    {
        music.volume = volume;
    }

    public void ChangeSoundVolume(float volume)
    {
        clickSound.volume = volume;
    }

    public void PlayClickSound()
    {
        clickSound.Play();
    }

}


