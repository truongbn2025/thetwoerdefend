using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using TMPro;
public class QualitySettingsManager : MonoBehaviour
{
    [SerializeField] private TMP_Dropdown dropdown;
    [SerializeField] private UniversalRenderPipelineAsset highQualityAsset;
    [SerializeField] private UniversalRenderPipelineAsset mediumQualityAsset;
    [SerializeField] private UniversalRenderPipelineAsset lowQualityAsset;

    public void SetURPAsset(int index)
    {
        //Specific setting, maybe not used
        switch (index)
        {
            case 0:
                break; 
            case 1:
                break; 
            case 2:
                break;
        }
        QualitySettings.SetQualityLevel(index);
    }
}
