using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MotherShip : MonoBehaviour
{
    [SerializeField] private float health = 200f;
    private float maxHealth = 200f;
    public Slider healthBar;
    public delegate void DeathEvent();
    public static event DeathEvent OnDeath;
    private void Start()
    {
        health = maxHealth;
    }




    public void TakeDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            OnDeath?.Invoke();
            //Lose
        }
        healthBar.value = (float)health / maxHealth;
    }
}
