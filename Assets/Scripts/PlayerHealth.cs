using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    private float maxHealth = 100;
    public float health;
    private Animator animator;
    public Slider healthBar;
    public delegate void DeathEvent();
    public static event DeathEvent OnDeath;
    private void Start()
    {
        health = maxHealth;
        animator = GetComponent<Animator>();
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        healthBar.value = health / maxHealth;
        if (health <= 0)
        {
            //die;
            animator.SetBool("isDead", true);
            GetComponent<PlayerControllerTDG>().enabled = false;
            OnDeath?.Invoke();
        }
        //Debug.Log("Player"+health);
    }

}
