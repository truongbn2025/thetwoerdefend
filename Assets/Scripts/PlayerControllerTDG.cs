
using UnityEngine;
using UnityEngine.InputSystem;


enum CHARACTER_STATE
{
    IDLE,
    WALKING,
    RUNNING,
    MINING
}

public class PlayerControllerTDG : MonoBehaviour
{
    public Transform pickUpPosition;
    private Rigidbody rb;
    [SerializeField] private float walkSpeed = 2f;
    [SerializeField] private float runSpeed = 2f;

    public MyInputAction playerController;
    private InputAction move;
    private InputAction interact;
    private InputAction run;
    private InputAction upgrade;
    private InputAction look;

    //Skill
    private InputAction activeSkillOne;
    private InputAction activeSkillTwo;
    private InputAction activeSkillThree;
    //public GameObject skillOne;
    //public GameObject skillTwo;
    //public GameObject skillThree;


    private GameObject targetTurret;
    private GameObject miningPosition;

    private Animator animator;


    private Vector2 moveDirection;
    private float _targetRotation = 0.0f;
    public float RotationSmoothTime = 0.12f;
    private float _rotationVelocity;
    private float _verticalVelocity;
    private float _terminalVelocity = 53.0f;
    private Vector3 movement;

    private Vector2 lookDirection;
    
    private bool canPickUp;
    private bool isHolding;

    //Camera
    GameObject _mainCamera;
    private const float _threshold = 0.01f;
    public bool LockCameraPosition = false;
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public GameObject CinemachineCameraTarget;
    public float CameraAngleOverride = 0.0f;

    //State
    private CHARACTER_STATE currentState;
    private void OnEnable()
    {
        EnableInputSytem();
    }

    private void EnableInputSytem()
    {
        move = playerController.Player.Move;
        move.Enable();

        look = playerController.Player.Look;
        look.Enable();

        interact = playerController.Player.Interact;
        playerController.Player.Interact.performed += OnInteract;
        playerController.Player.Interact.canceled += OnInteract;
        interact.Enable();

        upgrade = playerController.Player.Upgrade;
        playerController.Player.Upgrade.performed += OnUpgrade;
        upgrade.Enable();

        run = playerController.Player.Run;
        run.Enable();

        activeSkillOne = playerController.Player.ActiveSkillOne;
        activeSkillOne.performed += OnPerformSkill;
        activeSkillOne.Enable();

        activeSkillTwo = playerController.Player.ActiveSkillTwo;
        activeSkillTwo.performed += OnPerformSkill;
        activeSkillTwo.Enable();

        activeSkillThree = playerController.Player.ActiveSkillThree;
        activeSkillThree.performed += OnPerformSkill;
        activeSkillThree.Enable();
    }
    private void DisableInputSystem()
    {
        move.Disable();
        look.Disable();
        interact.Disable();
        upgrade.Disable();
        run.Disable();
        activeSkillOne.Disable();
        activeSkillTwo.Disable();
        activeSkillThree.Disable();
    }
    public void OnUpgrade(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if(LevelManager.Instance.upgradeLocation.isUpgrading)
            {
                LevelManager.Instance.upgradeLocation.DisplayUpgradePanel();
                LevelManager.Instance.upgradeManager.upgradingTarget = LevelManager.Instance.upgradeLocation.upgradingTurret;
            }
        }
    }
    public void OnPerformSkill(InputAction.CallbackContext context)
    {
        switch (context.action.name) {
            case "ActiveSkillOne":
                RaycastHit hit;
                if (Physics.Raycast(pickUpPosition.position, Vector3.down, out hit, 5f))
                {
                    //Instantiate(skillOne, hit.point, Quaternion.identity);
                    
                    LevelManager.Instance.activeSkillManager.UsingSkillOne(hit.point);
                }
                break;
            case "ActiveSkillTwo":
                if (Physics.Raycast(pickUpPosition.position, Vector3.down, out hit, 5f))
                {
                    
                    LevelManager.Instance.activeSkillManager.UsingSkillTwo(hit.point);
                    //Debug.Log("skill2");
                    //Instantiate(skillTwo, hit.point, Quaternion.identity);

                }
                break;
            case "ActiveSkillThree":
                if (Physics.Raycast(pickUpPosition.position, Vector3.down, out hit, 5f))
                {
                    //Instantiate(skillThree, hit.point, Quaternion.identity);

                }
                break;
        }
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            
            if (currentState == CHARACTER_STATE.MINING)
            {
                miningPosition.GetComponent<MiningLocation>().StartMining();
                //Debug.Log("pressed");
            }
            else
            {
                if (isHolding)
                {
                    if (targetTurret != null)
                        DeployTurret();
                }
                else
                {
                    if (targetTurret != null)
                        PickUpTurret();
                }
            }
            
        }
        else if (context.canceled)
        {
            // Key/Button is released up
            //Debug.Log("Jump key/button is released up");
        }
    }
    private void OnDisable()
    {
        playerController.Player.Interact.performed -= OnInteract;
        playerController.Player.Interact.canceled -= OnInteract;
        playerController.Player.Upgrade.performed -= OnUpgrade;
        DisableInputSystem();
    }
    private void Awake()
    {
        if (_mainCamera == null)
        {
            _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        }
        animator = GetComponent<Animator>();
        playerController = new MyInputAction();
    }
    void Start()
    {
        currentState = CHARACTER_STATE.IDLE;
        rb = GetComponent<Rigidbody>();
    }

    
    void Update()
    {
        lookDirection = look.ReadValue<Vector2>();
        moveDirection = move.ReadValue<Vector2>();
        movement = new Vector3(moveDirection.x, 0, moveDirection.y);

        //if (interact.performed)
        //{
        //    if(isHolding)
        //    {
        //        DeployTurret();
        //    }
        //    else
        //    {
        //        PickUpTurret();
        //    }
        //}

        //if(isHolding && interact.IsPressed()) {
        //    DeployTurret();
        //    Debug.Log("here");
        //}else if(!isHolding && canPickUp && interact.IsPressed())
        //{
        //    PickUpTurret();
        //    Debug.Log("asdsadhere");
        //}
        //HandleAnimation();
        //MoveChacter();
        
        switch(currentState)
        {
            case CHARACTER_STATE.IDLE:
                if (move.IsPressed())
                {
                    if (run.IsPressed())
                    {
                        TransitionToState(CHARACTER_STATE.RUNNING);
                    }
                    else
                    {
                        TransitionToState(CHARACTER_STATE.WALKING);
                    }
                }
                break;
            case CHARACTER_STATE.WALKING:

                if (run.IsPressed())
                {
                    TransitionToState(CHARACTER_STATE.RUNNING);
                }
                if (!move.IsPressed())
                {
                    TransitionToState(CHARACTER_STATE.IDLE);
                }
                break;
            case CHARACTER_STATE.RUNNING:
               
                if (!run.IsPressed())
                {
                    TransitionToState(CHARACTER_STATE.WALKING);
                }
                break;
            //case CHARACTER_STATE.LIFTING:
            //    //if(interact)
                break;
            case CHARACTER_STATE.MINING:
                if (move.IsPressed())
                {
                    if (run.IsPressed())
                    {
                        TransitionToState(CHARACTER_STATE.RUNNING);
                    }
                    else
                    {
                        TransitionToState(CHARACTER_STATE.WALKING);
                    }
                }
                break;
        }

    }

    private void FixedUpdate()
    {
        //RotateCharacter();
        switch (currentState)
        {
            case CHARACTER_STATE.IDLE:
                
                break;
            case CHARACTER_STATE.WALKING:
                //rb.velocity = new Vector3(moveDirection.x * walkSpeed, 0, moveDirection.y * walkSpeed);
                RotateCharacter(CHARACTER_STATE.WALKING);
                break;
            case CHARACTER_STATE.RUNNING:
                //rb.velocity = new Vector3(moveDirection.x * runSpeed, 0, moveDirection.y * runSpeed);
                RotateCharacter(CHARACTER_STATE.RUNNING);
                break;
            case CHARACTER_STATE.MINING:
                
                break;
                //case CHARACTER_STATE.LIFTING:

                //    break;
        }
        //MoveCharacter();
    }

    private void LateUpdate()
    {
        CameraRotation();
    }

    private void CameraRotation()
    {
        // if there is an input and camera position is not fixed
        if (lookDirection.sqrMagnitude >= _threshold && !LockCameraPosition)
        {
            //Don't multiply mouse input by Time.deltaTime;
            float deltaTimeMultiplier = 1.0f;

            _cinemachineTargetYaw += playerController.Player.Look.ReadValue<Vector2>().x * deltaTimeMultiplier;
            _cinemachineTargetPitch += playerController.Player.Look.ReadValue<Vector2>().y * deltaTimeMultiplier;
        }

        // clamp our rotations so our values are limited 360 degrees
        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        // Cinemachine will follow this target
        CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);
    }
    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
    private void TransitionToState(CHARACTER_STATE newState)
    {
        // Perform any necessary exit logic for the current state
        switch (currentState)
        {
            case CHARACTER_STATE.IDLE:
                // Exit idle state
                break;

            case CHARACTER_STATE.WALKING:
                // Exit walking state
                break;

            case CHARACTER_STATE.RUNNING:
                // Exit running state
                break;

            //case CHARACTER_STATE.LIFTING:
            //    // Exit jumping state
            //    break;

            
        }

        // Update the current state
        currentState = newState;

        // Perform any necessary entry logic for the new state
        switch (newState)
        {
            case CHARACTER_STATE.IDLE:
               
                animator.SetBool("isWalking", false);
                animator.SetBool("isRunning", false);
                
                break;

            case CHARACTER_STATE.WALKING:
                
                animator.SetBool("isWalking", true);
                animator.SetBool("isRunning", false);
                
                break;

            case CHARACTER_STATE.RUNNING:
                
                animator.SetBool("isWalking", false);
                animator.SetBool("isRunning", true);
                
                break;

            //case CHARACTER_STATE.LIFTING:
                
               
            //    animator.SetBool("isLifting", true);
            //    break;
            case CHARACTER_STATE.MINING:
                animator.SetBool("isWalking", false);
                animator.SetBool("isRunning", false);

                break;
        }
    }


    private void RotateCharacter(CHARACTER_STATE state)
    {
        Vector3 inputDirection = new Vector3(moveDirection.x, 0.0f, moveDirection.y).normalized;

        if (movement != Vector3.zero)
        {
            _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                              _mainCamera.transform.eulerAngles.y;
            float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                RotationSmoothTime);

            // rotate to face input direction relative to camera position
            transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
        }
        Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;
        
        // move the player
        if(state == CHARACTER_STATE.WALKING)
        {
            rb.velocity = new Vector3(targetDirection.x * walkSpeed, targetDirection.y * runSpeed, targetDirection.z * walkSpeed);
        }
        else if(state == CHARACTER_STATE.RUNNING)
        {
            rb.velocity = new Vector3(targetDirection.x * runSpeed, targetDirection.y * runSpeed, targetDirection.z * runSpeed);

        }

        //if (movement != Vector3.zero)
        //{
        //    Quaternion toRotation = Quaternion.LookRotation(movement, Vector3.up);
        //    transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, 200f * Time.deltaTime);
        //}
    }



    private void DeployTurret()
    {
        RaycastHit hit;
        if(Physics.Raycast(pickUpPosition.position, Vector3.down, out hit, 5f))
        {
            if (hit.transform.CompareTag("UpgradeLocation"))
            {
                animator.SetBool("isLifting", false);
                isHolding = false;
                targetTurret.transform.position = hit.transform.position;
                targetTurret.transform.SetParent(null, true);
                //targetTurret.GetComponent<MachineGun>().IdleTurret();
                targetTurret.GetComponent<MachineGun>().UpgradeTurret();
                LevelManager.Instance.upgradeLocation.upgradingTurret = targetTurret;
                LevelManager.Instance.upgradeLocation.DisplayHelper(true);
            }
            else
            {
                //Debug.Log(hit.transform.name);
                animator.SetBool("isLifting", false);
                isHolding = false;
                targetTurret.transform.position = hit.point;
                targetTurret.transform.SetParent(null, true);
                targetTurret.GetComponent<MachineGun>().IdleTurret();
            }
            
        }


        

        ////targetTurret.transform.position = pickUpPosition.transform.position;
        //targetTurret.transform.SetParent(null, true);
        
    }

    private void PickUpTurret()
    {
        RaycastHit hit;
        if (Physics.Raycast(pickUpPosition.position, Vector3.down, out hit, 5f))
        {
            if (hit.transform.CompareTag("UpgradeLocation"))
                LevelManager.Instance.upgradeLocation.DisplayHelper(false);
        }

        animator.SetBool("isLifting", true);
        //canPickUp = false;
        isHolding = true;
        targetTurret.transform.position = pickUpPosition.transform.position;
        targetTurret.transform.SetParent(pickUpPosition, true);
        targetTurret.GetComponent<MachineGun>().RelocateTurret();
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("UpgradeMaterial") && !isHolding)
        {
            other.GetComponent<MiningLocation>().DisplayHelper(true);
            
            TransitionToState(CHARACTER_STATE.MINING);
            miningPosition = other.gameObject;
        }


        if (other.CompareTag("Turret") && targetTurret == null)
        {
            canPickUp = true;
            targetTurret = other.gameObject;
            //Debug.Log("canPick");
            //PickUpTurret();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("UpgradeMaterial") && !isHolding)
        {
            //other.GetComponent<MiningLocation>().DisplayHelper(true);

            TransitionToState(CHARACTER_STATE.MINING);
            
            miningPosition = other.gameObject;
        }


        if (other.CompareTag("Turret") && targetTurret == null)
        {
            canPickUp = true;
            targetTurret = other.gameObject;
            //Debug.Log("canPick");
            //PickUpTurret();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("UpgradeMaterial"))
        {
            other.GetComponent<MiningLocation>().StopMining();
            other.GetComponent<MiningLocation>().DisplayHelper(false);
            miningPosition = null;
        }
        if (other.CompareTag("Turret") && other.gameObject == targetTurret)
        {
            canPickUp = false;
            targetTurret = null;
            //PickUpTurret();
            
        }
    }
}
