using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    //MenuUIManager menuUIManager;
    public CurrencySystem currencySystem;
    public SelectLevel selectLevel;
    public QualitySettingsManager qualitySettingsManager;
    public GeneralUpgradeManager generalUpgradeManager;
    

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //menuUIManager = GetComponent<MenuUIManager>();
        currencySystem = GetComponent<CurrencySystem>();
        selectLevel = GetComponent<SelectLevel>();
        qualitySettingsManager = GetComponent<QualitySettingsManager>();
        generalUpgradeManager = GetComponent<GeneralUpgradeManager>();
    }

}
