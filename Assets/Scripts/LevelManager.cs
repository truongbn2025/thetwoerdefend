using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public struct LevelCollected{
    public int enemyKiled;
    public int towerUpgraded;
    public int coinCollected;
}

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }



    public UpgradeLocation upgradeLocation;
    public MiningLocation miningLocation;

    public ActiveSkillManager activeSkillManager;
    public UpgradeManager upgradeManager;


    //Not using yet
    public GameObject character;
    public GameObject turretPrefabs;
    //---------------------
    public LevelCollected levelCollected;
    public int upgradeMaterial;
    public int collectedCoin;
    public TMP_Text displayUpgradeMaterial;
    public TMP_Text displayCoinCollected;
    public int enemyLeft;
    public List<Spawner> spawners;

    //UI need UIManager later;
    public GameObject endGamePanel;
    public int upgradeNumber;

    private void Awake()
    {
        
        if (Instance == null)
        {
            Instance = this;
            
        }
        else
        {
            Destroy(gameObject);
        }
        Cursor.visible = false;
        upgradeNumber = 0;
        enemyLeft = 0;
        levelCollected = new LevelCollected();
        //upgradeManager = GetComponent<UpgradeManager>();
    }
    private void OnEnable()
    {
        PlayerHealth.OnDeath += Lose;
        MotherShip.OnDeath += Lose;
        UpgradeManager.OnUpgrade += Upgraded;
        MiningLocation.OnMined += AddUpgradeMaterial;
        EnemyHealth.OnEnemyDead += UpdateEnemyNumber;
    }

    private void OnDisable()
    {
        PlayerHealth.OnDeath -= Lose;
        MotherShip.OnDeath -= Lose;
        UpgradeManager.OnUpgrade -= Upgraded;
        MiningLocation.OnMined -= AddUpgradeMaterial;
        EnemyHealth.OnEnemyDead -= UpdateEnemyNumber;
    }


    void Start()
    {
        upgradeMaterial = 0;
    }

    // Update is called once per frame
    void Update()
    {
        displayUpgradeMaterial.text = upgradeMaterial.ToString();
        displayCoinCollected.text = collectedCoin.ToString();
    }

    private void Upgraded()
    {
        upgradeNumber++;
    }

    void Win()
    {
        endGamePanel.SetActive(true);
        endGamePanel.transform.GetChild(0).GetComponent<TMP_Text>().text = "You Win!";
        endGamePanel.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = levelCollected.enemyKiled.ToString();
        endGamePanel.transform.GetChild(1).GetChild(1).GetComponent<TMP_Text>().text = upgradeNumber.ToString();
        endGamePanel.transform.GetChild(1).GetChild(2).GetComponent<TMP_Text>().text = collectedCoin.ToString();
        GameManager.instance.currencySystem.AddCoin(collectedCoin);
        Cursor.visible = true;
        GameManager.instance.currencySystem.AddCoin(collectedCoin);
    }
    void Lose()
    {
        endGamePanel.SetActive(true);
        endGamePanel.transform.GetChild(0).GetComponent<TMP_Text>().text = "You Lose!";
        endGamePanel.transform.GetChild(1).GetChild(0).GetComponent<TMP_Text>().text = levelCollected.enemyKiled.ToString();
        endGamePanel.transform.GetChild(1).GetChild(1).GetComponent<TMP_Text>().text = upgradeNumber.ToString();
        endGamePanel.transform.GetChild(1).GetChild(2).GetComponent<TMP_Text>().text = collectedCoin.ToString();
        Cursor.visible = true;
    }

    public void LoadMenu()
    {
        GameManager.instance.selectLevel.Select(0);
        SoundManager.Instance.PlayClickSound();
    }

    void UpdateEnemyNumber()
    {
        //Only enemy killed, enemy add in Spawner.cs
        levelCollected.enemyKiled++;
        enemyLeft--;
        foreach (Spawner spawner in spawners)
        {
            if(spawner.waveState != WAVE_STATE.COUNTING)
            {
                return;
            }
            else
            {
                continue;
            }
        }

        if (enemyLeft <= 0)
        {
            //Debug.Log("zero");
            foreach(Spawner spawner in spawners)
            {
                if(spawner.nextWave < spawner.waves.Count)
                {
                    //to nextwave
                    //Debug.Log("asdsa" + spawner.nextWave);
                    spawner.TransitionToState(WAVE_STATE.WAITING);
                    if (spawner.nextWave == spawner.waves.Count-1)
                    {
                        //end game win;
                        //Debug.Log("Last wave");
                    }
                }
                else if(spawner.nextWave == spawner.waves.Count)
                {
                    //Debug.Log("End game win");
                    Win();
                }
            }
        }
    }

    void AddUpgradeMaterial()
    {
        Debug.Log("upgradeMaterial" + upgradeMaterial);
        upgradeMaterial ++;
        
        Debug.Log("upgradeMaterial" + upgradeMaterial);
    }
}
