
using UnityEngine;
using UnityEngine.EventSystems;
using System;
public class ButtonBehaviour : MonoBehaviour, IPointerDownHandler
{
    //Upgrade ingame Button Behavior
    public delegate void ClickButtonEvent(UPGRADE_TYPE type);
    public static event ClickButtonEvent OnClickButton;
    public UPGRADE_TYPE myType;
    public void OnPointerDown(PointerEventData eventData)
    {
        OnClickButton?.Invoke(myType);
    }
}
