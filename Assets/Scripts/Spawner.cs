using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Wave
{
    
    public List<GameObject> waveEnemies;
    public int enemyCount;
    public float spawnDelayTime;

   
}

public enum WAVE_STATE
{
    WAITING,
    SPAWNING,
    COUNTING,
}

public class Spawner : MonoBehaviour
{
    public List<Wave> waves;
    public float timeBetweenWaves;
    //public int enemyLeft;
    
    private float wavesCountdown;

    public WAVE_STATE waveState;
    public Transform destination;
    public int nextWave;

    private void Start()
    {
        if(destination == null)
        {
            destination = FindObjectOfType<MotherShip>().transform;
        }
        //enemyLeft = 0;
        waveState = WAVE_STATE.WAITING;
        wavesCountdown = timeBetweenWaves;
        nextWave = 0;
    }

    private void Update()
    {
        if(nextWave < waves.Count)
        {
            switch (waveState)
            {
                case WAVE_STATE.WAITING:
                    WaitForNextWave();
                    break;
                case WAVE_STATE.SPAWNING:
                    //if (nextWave < waves.Count)
                    //    StartSpawning(nextWave);
                    //else
                    //    Debug.Log("there is no wave left");
                    break;
                case WAVE_STATE.COUNTING:
                    if (LevelManager.Instance.enemyLeft > 0)
                    {
                        //Debug.Log("counting " + LevelManager.Instance.enemyLeft);

                        //TransitionToState(WAVE_STATE.WAITING);
                    }
                    else
                    {
                        //TransitionToState(WAVE_STATE.WAITING);
                    }
                    break;
            }
        }
        else
        {
            //EndLevel();
            //Debug.Log("Level Ended");
        }
        
    }


    void WaitForNextWave()
    {
        if(wavesCountdown <= 0)
        {
            //Start Next Wave
            TransitionToState(WAVE_STATE.SPAWNING);
        }
        else
        {
            wavesCountdown -= Time.deltaTime;
        }
        //Debug.Log(wavesCountdown);
    }

    void StartSpawning(int waveNumber)
    {
        Debug.Log("Spawning wave " + waveNumber);
        for(int i = 0; i < waves[waveNumber].enemyCount; i++)
        {
            float timeDelay = (i+1) * waves[waveNumber].spawnDelayTime;
            if(i == (waves[waveNumber].enemyCount - 1))
            {
                StartCoroutine(SpawnEnemy(timeDelay, waves[waveNumber], true));
            }
            else
            {
                StartCoroutine(SpawnEnemy(timeDelay, waves[waveNumber], false));
            }

            //StartCoroutine(SpawnEnemy(timeDelay, waves[waveNumber]));
            //if (i == (waves[waveNumber].enemyCount - 1))
            //{
            //    StartCoroutine(TransitionToCountingState(timeDelay));
                
            //}
            
        }
        nextWave++;
    }
    
    IEnumerator SpawnEnemy(float timeDelay, Wave thisWave, bool lastMember)
    {
        yield return new WaitForSeconds(timeDelay);
        
        
        LevelManager.Instance.enemyLeft++;
        int enemyType = Random.Range(0, thisWave.waveEnemies.Count);
        Debug.Log(enemyType);
        var newEnemy = Instantiate(thisWave.waveEnemies[enemyType], transform.position, Quaternion.identity);
        newEnemy.transform.GetComponent<MonsterNavmesh>().destination = destination;
        if (lastMember)
        {
            TransitionToState(WAVE_STATE.COUNTING);
        }
        //Debug.Log("Spawned at: " + Time.time);
    }

    IEnumerator TransitionToCountingState(float timeDelay)
    {
        yield return new WaitForSeconds(timeDelay);
        TransitionToState(WAVE_STATE.COUNTING);
        //Debug.Log("Spawned at: " + Time.time);
    }

    public void TransitionToState(WAVE_STATE newWaveState)
    {
        waveState = newWaveState;
        switch (waveState)
        {
            case WAVE_STATE.WAITING:
                wavesCountdown = timeBetweenWaves;
                break;
            case WAVE_STATE.SPAWNING:
                StartSpawning(nextWave);
                break;
            case WAVE_STATE.COUNTING:
                
                break;
        }

    }
}
