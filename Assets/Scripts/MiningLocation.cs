using System;
using UnityEngine;
using UnityEngine.UI;

public class MiningLocation : MonoBehaviour
{
    public delegate void MiningEvent();
    public static event MiningEvent OnMined;
    //public static event Action<int> OnMinedMaterial;

    public float miningTime = 2f;
    private float timer = 0f;
    private bool isMining;
    public GameObject helper;
    public GameObject progressBar;
    public Slider progressBarSlider;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isMining)
        {
            progressBarSlider.value = (float)timer/miningTime;
            if(timer < miningTime)
            {
                timer += Time.deltaTime;
            }
            else
            {
                OnMined?.Invoke();
                timer = 0;
                Debug.Log("mined " + Time.time);
            }
        }
    }

    public void StartMining()
    {
        helper.SetActive(false);
        progressBar.SetActive(true);
        isMining = true;
    }
    public void StopMining()
    {
        helper.SetActive(false);
        progressBar.SetActive(false);
        isMining = false;
    }
    public void DisplayHelper(bool state)
    {
        helper.SetActive(state);
    }
}
