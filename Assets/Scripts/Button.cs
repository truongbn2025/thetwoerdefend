using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public enum BUTTON_TYPE
{
    LEVEL,
    UPGRADE,
    MENU_UI
}
public class Button : MonoBehaviour, IPointerDownHandler
{
    public GameManager gameManager;
    public SoundManager soundManager;
    public MenuUIManager menuUIManager;
    public BUTTON_TYPE buttonType;

    public MENU_UI_STATE menuUIState;
    public UPGRADE_TYPE upgradeType;
    public int level;
    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        soundManager = FindObjectOfType<SoundManager>();
        menuUIManager = FindObjectOfType<MenuUIManager>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        soundManager.PlayClickSound();
        switch (buttonType)
        {
            case BUTTON_TYPE.LEVEL:
                gameManager.selectLevel.Select(level);
                break;
            case BUTTON_TYPE.UPGRADE:
                Upgrade(upgradeType);
                break; 
            case BUTTON_TYPE.MENU_UI:
                NavigateMenu(menuUIState);
                break;
        }
    }
    private void NavigateMenu(MENU_UI_STATE state)
    {
        switch(state)
        {
            case MENU_UI_STATE.CHOOSE_LEVEL:
                menuUIManager.ChooseLevel();
                break;
            case MENU_UI_STATE.UPGRADE:
                menuUIManager.Upgrade();
                break;
            case MENU_UI_STATE.SETTINGS:
                menuUIManager.Settings();
                break;
            case MENU_UI_STATE.MAIN_MENU:
                menuUIManager.BackToMainMenu();
                break;
            case MENU_UI_STATE.EXIT:
                menuUIManager.Exit();
                break;
        }
    }
    private void Upgrade(UPGRADE_TYPE type)
    {
        switch(type) {
            case UPGRADE_TYPE.ATTACK:
                gameManager.generalUpgradeManager.UpgradeDamage();
                break;
            case UPGRADE_TYPE.FIRE_RANGE:
                gameManager.generalUpgradeManager.UpgradeFireRange();
                break;
            case UPGRADE_TYPE.FIRE_RATE:
                gameManager.generalUpgradeManager.UpgradeFireRate();
                break;
        }
    }
}
