using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSlider : MonoBehaviour
{
    private Slider slider;

    private void Awake()
    {

        slider = GetComponent<Slider>();
    }

    private void Start()
    {
        SoundManager.Instance.ChangeSoundVolume(slider.value);
        slider.onValueChanged.AddListener((val) => SoundManager.Instance.ChangeSoundVolume(val));
    }
}
