using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Turret : MonoBehaviour
{
    #region Variables
    [Header("Basic Stats")]
    public float damage;
    public float fireRate;
    public float fireRange;

    //public bool useAmmo;
    public int maxAmmoClip;
    public int clipSize;
    public float reloadTime;
    public float rotateSpeed = 5f;

    public string enemyTag = "Enemy";
    public string secondaryEnemyTag = "SecondEnemy";
    [Header("Prefabs")]
    public GameObject projectile;
    public GameObject muzzleSpark;
    public GameObject hitEffect;
    public Transform enemyDetector;

    [Header("Turret Parts")]
    public Transform turretPivot; //rotate y axis
    public Transform turretHead; //rotate z axis
    public Transform[] muzzlePositions; //position to fire from
    
    [Tooltip("Minimum angle limit.")]
    public float minVerticalAngle = -30;
    [Tooltip("Maximum angle limit.")]
    public float maxVerticalAngle = 70;


    //private components
    protected float bulletSpeed; //Speed of projectiles
    protected GameObject target; //Enemy Target
    protected int ammoLeft = 1000;
    private Vector3 aimDirection;
    private Quaternion pivotRotation;   //x rotation
    private Quaternion headRotation;    //y rotation

    #endregion

    #region Start Update
    private void OnEnable()
    {
        EnemyHealth.OnEnemyDead += CheckKill;
    }

    private void OnDisable()
    {
        EnemyHealth.OnEnemyDead -= CheckKill;
    }

    protected void Start()
    {
        
        enemyDetector.GetComponent<SphereCollider>().radius = fireRange;
        
    }
    #endregion
    
    #region Trigger
    protected void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(enemyTag) || other.CompareTag(secondaryEnemyTag))
        {
            LockOnTarget(other.gameObject);
            //Debug.Log("parent");
            
        }
    }

    protected void OnTriggerStay(Collider other)
    {
        if (other.CompareTag(enemyTag) || other.CompareTag(secondaryEnemyTag))
        {
            //Debug.Log("Staying");
            //LockOnTarget();
            if(target == null)
            {
                target = other.gameObject;
            }
            if(Vector3.Distance(other.transform.position, transform.position) < Vector3.Distance(target.transform.position, transform.position))
            {
                //LockOnTarget(other.gameObject);
                target = other.gameObject;
            }
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(enemyTag) || other.CompareTag(secondaryEnemyTag))
        {
            if(other.gameObject == target)
            {
                target = null;
            }
        }
    }
    #endregion

    #region Methods
    private void CheckKill()
    {
        target = null;
        
        //target = null;
        //if (deadTarget == target)
        //{
        //    Debug.Log("I killed this guy");

        //}

    }
    public void LockOnTarget(GameObject probableTarget)
    {
        if (probableTarget.CompareTag(enemyTag) )
        {
            if(target == null)
            {
                target = probableTarget;
                
            }
            
        }
        else
        {
            target = null;
            
        }
            
    }

    protected void CalculateAimDirection(Vector3 aimTargetPosition)
    {
        aimDirection = aimTargetPosition - turretHead.transform.position;
        pivotRotation = headRotation = Quaternion.LookRotation(aimDirection);
        
        pivotRotation = Quaternion.Euler(pivotRotation.eulerAngles);
        pivotRotation.x = 0; //neutralize the rotation on X
        pivotRotation.z = 0; //and Z
        
        if (minVerticalAngle != 0 || maxVerticalAngle != 0) //if we have angle limits set, calculate the clamped value of X
        {
            Vector3 eulerRot = headRotation.eulerAngles; //get the rotation in euler angles
            eulerRot = new Vector3(ClampAngleAI(eulerRot.x), eulerRot.y, 0); //clamp it
            headRotation = Quaternion.Euler(eulerRot); //set it as the new desired head rotation
        }
    }

    public void LookAtEnemy()
    {
        Debug.DrawRay(transform.position, transform.forward);
        //Quaternion rotation = Quaternion.LookRotation(aimDirection);
        if (turretPivot) //(for rotation on Y axis)
            turretPivot.rotation = Quaternion.Lerp(turretPivot.rotation, pivotRotation, Time.deltaTime * rotateSpeed);
        if (turretHead) //(for rotation on X axis)
            turretHead.rotation = Quaternion.Lerp(turretHead.rotation, headRotation, Time.deltaTime * rotateSpeed); ;
        
    }
    //calculate angle limits, and return the new clamped angle values
    float ClampAngleAI(float angle)
    {
        if (angle < 360f + minVerticalAngle && angle > 270)
            angle = 360f + minVerticalAngle;
        else if (angle > maxVerticalAngle && angle < 360f + minVerticalAngle)
            angle = maxVerticalAngle;
        return angle;
    }

    #endregion
}
