using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GeneralUpgradeManager : MonoBehaviour
{
    public int damageUpgrade;
    public int fireRateUpgrade;
    public int fireRangeUpgrade;

    public MenuUIManager menuUIManager;
    private void Awake()
    {
        if (PlayerPrefs.HasKey("DamageUpgrade"))
            damageUpgrade = PlayerPrefs.GetInt("DamageUpgrade");
        else
        {
            damageUpgrade = 0;
            PlayerPrefs.SetInt("DamageUpgrade", 0);
        }

        if (PlayerPrefs.HasKey("FireRateUpgrade"))
            fireRateUpgrade = (int)PlayerPrefs.GetInt("FireRateUpgrade");
        else
        {
            fireRateUpgrade = 0;
            PlayerPrefs.SetInt("FireRateUpgrade", 0);
        }

        if (PlayerPrefs.HasKey("FireRangeUpgrade"))
            fireRangeUpgrade = (int)PlayerPrefs.GetInt("FireRangeUpgrade");
        else
        {
            fireRangeUpgrade = 0;
            PlayerPrefs.SetInt("FireRangeUpgrade", 0);
        }
        
        
    }
 
    public void UpgradeDamage()
    {
        if(GameManager.instance.currencySystem.GetCointAmmount() > damageUpgrade)
        {
            GameManager.instance.currencySystem.DecreaseCoin(damageUpgrade + 1);
            damageUpgrade++;
            PlayerPrefs.SetInt("DamageUpgrade", damageUpgrade);
            if (menuUIManager != null)
            {
                menuUIManager.UpdateUI();
            }
        }
        
    }
    public void UpgradeFireRate()
    {
        if (GameManager.instance.currencySystem.GetCointAmmount() > fireRateUpgrade)
        {
            GameManager.instance.currencySystem.DecreaseCoin(fireRateUpgrade + 1);
            fireRateUpgrade++;
            PlayerPrefs.SetInt("FireRateUpgrade", fireRateUpgrade);
            if (menuUIManager != null)
            {
                menuUIManager.UpdateUI();
            }
        }
    }
    public void UpgradeFireRange()
    {
        if (GameManager.instance.currencySystem.GetCointAmmount() > fireRangeUpgrade)
        {
            GameManager.instance.currencySystem.DecreaseCoin(fireRangeUpgrade + 1);
            fireRangeUpgrade++;
            PlayerPrefs.SetInt("FireRangeUpgrade", fireRangeUpgrade);
            if (menuUIManager != null)
            {
                menuUIManager.UpdateUI();
            }
        }
    }
}
