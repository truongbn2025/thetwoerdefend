using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencySystem : MonoBehaviour
{
    private int coin;

    private void Awake()
    {
        if (PlayerPrefs.HasKey("Coin"))
            coin = PlayerPrefs.GetInt("Coin");
        else
        {
            coin = 10;
            PlayerPrefs.SetInt("Coin", coin);
        }

        
    }

    public void SetCoin(int ammount)
    {
        coin = ammount;
        PlayerPrefs.SetInt("Coin", coin);
    }
    public void DecreaseCoin(int ammount)
    {
        coin -= ammount;
        PlayerPrefs.SetInt("Coin", coin);
    }

    public void AddCoin(int ammount)
    {
        coin += ammount;
        PlayerPrefs.SetInt("Coin", coin);
    }

    public int GetCointAmmount()
    {
        return coin;
    }
}
