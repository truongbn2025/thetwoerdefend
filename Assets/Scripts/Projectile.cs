using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector3 targetPosition;
    public GameObject impactEffect;
    public float damage;

    private Rigidbody thisRb;
    private Collider thisCollider;
    private Vector3 previousPosition;
    [SerializeField] private float projectileSpeed = 10f;
    private void Start()
    {
        thisRb = GetComponent<Rigidbody>();
        thisCollider = GetComponent<Collider>();
        transform.LookAt(targetPosition);
    }

    private void FixedUpdate()
    {
        
        thisRb.AddForce(transform.forward * projectileSpeed);
        //CheckCollision(previousPosition);

        //previousPosition = transform.position;
    }

    //void CheckCollision(Vector3 prevPos)
    //{
    //    RaycastHit hit;
    //    Vector3 direction = transform.position - prevPos;
    //    Ray ray = new Ray(prevPos, direction);
    //    float dist = Vector3.Distance(transform.position, prevPos);
    //    if (Physics.Raycast(ray, out hit, dist))
    //    {
    //        transform.position = hit.point;
    //        Quaternion rot = Quaternion.FromToRotation(Vector3.forward, hit.normal);
    //        Vector3 pos = hit.point;
    //        Instantiate(impactEffect, pos, rot);
    //        Destroy(gameObject);

    //    }
    //}

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            //Debug.Log(collision.transform.name);
            ContactPoint contact = collision.contacts[0];
            Quaternion rot = Quaternion.FromToRotation(Vector3.forward, contact.normal);
            
            Vector3 pos = contact.point;
            
            Instantiate(impactEffect,pos, rot, collision.transform);
            //GameObject temp = Instantiate(impactEffect, pos, rot);
            //temp.transform.SetParent(collision.transform, false);
            Destroy(gameObject);
            collision.transform.GetComponentInParent<EnemyHealth>().TakeDamage(damage);
            
        }    
    }
}
