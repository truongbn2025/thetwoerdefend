using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public enum ENEMY_STATE
{
    IDLE,
    WALKING,
    ATTACKING,
    DIE
}

public class MonsterNavmesh : MonoBehaviour
{
    public Transform destination;
    private Animator animator;
    private NavMeshAgent navAgent;
    public ENEMY_STATE enemyState;
    private bool canStartMoving;
    private GameObject target;
    private float idleTime;
    private float damage = 1f;

    private string playerTag;
    private const string PLAYER_TAG = "Player";

    private void Awake()
    {
        playerTag = PLAYER_TAG;
        canStartMoving= true;
        idleTime = 3f;
        animator = GetComponent<Animator>();
        enemyState = ENEMY_STATE.IDLE;
        navAgent = GetComponent<NavMeshAgent>();
        destination = FindObjectOfType<MotherShip>().transform;
    }

    private void Update()
    {
        switch (enemyState)
        {
            case ENEMY_STATE.IDLE:
                animator.SetBool("isWalking", false);
                animator.SetBool("isAttacking", false);
                if (canStartMoving)
                {
                    StartCoroutine(StartToMove(idleTime));
                }
                break;
            case ENEMY_STATE.WALKING:
                MoveToDestination();
                break;
            case ENEMY_STATE.ATTACKING:
                gameObject.transform.LookAt(target.transform);
                //EnemyAttack(); 
                break;
            case ENEMY_STATE.DIE:
                
                break;
        }

        //navAgent.destination = destination.position;
    }



    IEnumerator StartToMove(float time)
    {
        canStartMoving = false;
        yield return new WaitForSeconds(time);
        TransitionToState(ENEMY_STATE.WALKING);
    }

    IEnumerator GetDestroyed()
    {
        GetComponentInChildren<Collider>().enabled = false;
        yield return new WaitForSeconds(2.5f);
        Destroy(gameObject);
    }

    private void MoveToDestination()
    {
        navAgent.destination = destination.position;
    }

    private void StopMoving()
    {
        navAgent.destination = transform.position;
    }


    public void TransitionToState(ENEMY_STATE state)
    {
        switch (enemyState)
        {
            case ENEMY_STATE.IDLE:
                
                break;
            case ENEMY_STATE.WALKING:
                StopMoving();
                break;
            case ENEMY_STATE.ATTACKING:
                StopMoving();
                break;
            case ENEMY_STATE.DIE:
                
                break;
        }


        enemyState = state;
        switch (enemyState)
        {
            case ENEMY_STATE.IDLE:
                animator.SetBool("isWalking", false);
                animator.SetBool("isAttacking", false);
                break;
            case ENEMY_STATE.WALKING:
                animator.SetBool("isWalking", true);
                animator.SetBool("isAttacking", false);
                break;
            case ENEMY_STATE.ATTACKING:
                animator.SetBool("isWalking", false);
                animator.SetBool("isAttacking", true);
                break;
            case ENEMY_STATE.DIE:
                StopMoving();
                animator.SetBool("isDead", true);
                StartCoroutine(GetDestroyed());
                break;
        }
    }

    void ApplyDamage()
    {
        if(target != null)
        {
            if (target.CompareTag("Player"))
                target.GetComponent<PlayerHealth>().TakeDamage(damage);
            if (target.CompareTag("MotherShip"))
                target.GetComponent<MotherShip>().TakeDamage(damage);
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(playerTag) || other.transform.CompareTag("MotherShip"))
        {
            Debug.Log(other.transform.name);
            gameObject.transform.LookAt(other.transform);
            TransitionToState(ENEMY_STATE.ATTACKING);
            target = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag(playerTag))
        {
            if (other.gameObject == target)
            {
                canStartMoving = true;
                idleTime = 1f;
                target = null;
                TransitionToState(ENEMY_STATE.IDLE);
            }
        }
        
    }

    public void GetDistracted()
    {
        playerTag = "IgnorePhysics";
        if(enemyState == ENEMY_STATE.ATTACKING)
        {
            canStartMoving = true;
            idleTime = 1f;
            target = null;
            TransitionToState(ENEMY_STATE.IDLE);
        }
        

    }

    public void GetOutOfDistractingArea()
    {
        playerTag = PLAYER_TAG;
        
    }
}
