using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class DistractEffect : Skill
{

    private void Start()
    {
        StartCoroutine(AutoDestroy());
    }

    IEnumerator AutoDestroy() { 
        yield return new WaitForSeconds(effectTime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            other.GetComponentInParent<MonsterNavmesh>().GetDistracted();

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            other.GetComponentInParent<MonsterNavmesh>().GetOutOfDistractingArea();

        }
    }
}

