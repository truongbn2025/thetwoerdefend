using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeLocation : MonoBehaviour
{
    //public static MiningLocation Instance;
    public float UpgradeTime = 2f;
    private float timer = 0f;
    public GameObject upgradingTurret;
    public bool isUpgrading;
    public GameObject helper;
    public GameObject progressBar;
    public Slider progressBarSlider;
    public GameObject upgradePanel;

    public void DisplayUpgradePanel()
    {
        upgradePanel.SetActive(!upgradePanel.active);
        Cursor.visible = upgradePanel.active;
    }

    public void DisplayHelper(bool state)
    {
        
        isUpgrading = true;
        helper.SetActive(state);
    }
}
