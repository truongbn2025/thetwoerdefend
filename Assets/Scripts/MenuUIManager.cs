using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
public enum MENU_UI_STATE
{
    MAIN_MENU,
    CHOOSE_LEVEL,
    UPGRADE,
    SETTINGS,
    EXIT,
}

public class MenuUIManager : MonoBehaviour
{
    public GameObject mainMenuButtonsPanel;
    public GameObject chooseLevelPanel;
    public GameObject upgradePanel;
    public GameObject settingPanel;
    public GameObject titlePanel;
    public GameObject coinPanel;

    public TMP_Text coinAmmount;
    public TMP_Text damageUpgrade;
    public TMP_Text fireRateUpgrade;
    public TMP_Text fireRangeUpgrade;

    public GameObject backButton;

    public Transform panelPosition;
    public Transform panelHidingPosition;

    public Transform backButtonPosition;
    public Transform backButtonHidingPosition;

    public Transform coinPosition;
    public Transform coinHidingPosition;

    public Transform titlePosition;
    public Transform titleHidingPosition;

    public float transitionTime;

    public MENU_UI_STATE state;

    private void Awake()
    {
        state = MENU_UI_STATE.MAIN_MENU;
    }
    private void Start()
    {
        UpdateUI();
    }
    public void ChooseLevel()
    {
        //mainMenuButtonsPanel.SetActive(false);
        mainMenuButtonsPanel.transform.DOMove(panelHidingPosition.position, transitionTime).OnComplete(() => {
            chooseLevelPanel.transform.DOMove(panelPosition.position, transitionTime);
            backButton.transform.DOMove(backButtonPosition.position, transitionTime);
        } );
        
        state = MENU_UI_STATE.CHOOSE_LEVEL;
    }

    public void Upgrade()
    {
        titlePanel.transform.DOMove(titleHidingPosition.position, transitionTime);
        mainMenuButtonsPanel.transform.DOMove(panelHidingPosition.position, transitionTime).OnComplete(() => {
            upgradePanel.transform.DOMove(panelPosition.position, transitionTime);
            backButton.transform.DOMove(backButtonPosition.position, transitionTime);
            coinPanel.transform.DOMove(coinPosition.position, transitionTime);
        });

        state = MENU_UI_STATE.UPGRADE;
    }

    public void Settings()
    {
        mainMenuButtonsPanel.transform.DOMove(panelHidingPosition.position, transitionTime).OnComplete(() => {
            settingPanel.transform.DOMove(panelPosition.position, transitionTime);
            backButton.transform.DOMove(backButtonPosition.position, transitionTime);
        });

        state = MENU_UI_STATE.SETTINGS;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void BackToMainMenu()
    {
        switch (state)
        {
            case MENU_UI_STATE.CHOOSE_LEVEL:
                chooseLevelPanel.transform.DOMove(panelHidingPosition.position, transitionTime);
                backButton.transform.DOMove(backButtonHidingPosition.position, transitionTime).OnComplete(() => mainMenuButtonsPanel.transform.DOMove(panelPosition.position, transitionTime));
                state = MENU_UI_STATE.MAIN_MENU;
                break;
            case MENU_UI_STATE.UPGRADE:
                upgradePanel.transform.DOMove(panelHidingPosition.position, transitionTime);
                coinPanel.transform.DOMove(coinHidingPosition.position, transitionTime);
                backButton.transform.DOMove(backButtonHidingPosition.position, transitionTime).OnComplete(() => {
                    mainMenuButtonsPanel.transform.DOMove(panelPosition.position, transitionTime);
                    titlePanel.transform.DOMove(titlePosition.position, transitionTime);
                    
                    });
                state = MENU_UI_STATE.MAIN_MENU;
                break;
            case MENU_UI_STATE.SETTINGS:
                settingPanel.transform.DOMove(panelHidingPosition.position, transitionTime);
                backButton.transform.DOMove(backButtonHidingPosition.position, transitionTime).OnComplete(() => mainMenuButtonsPanel.transform.DOMove(panelPosition.position, transitionTime));
                state = MENU_UI_STATE.MAIN_MENU;
                break;
        }
    }

    public void UpdateUI()
    {
        coinAmmount.text = GameManager.instance.currencySystem.GetCointAmmount().ToString();
        damageUpgrade.text = GameManager.instance.generalUpgradeManager.damageUpgrade.ToString();
        fireRateUpgrade.text = GameManager.instance.generalUpgradeManager.fireRateUpgrade.ToString();
        fireRangeUpgrade.text = GameManager.instance.generalUpgradeManager.fireRangeUpgrade.ToString();

    }
}
