using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//struct UpgradeInfomation
//{
//    public string name;
//    public string description;
//    public int cost;
//    public float effectiveness;
//}

public struct UpgradeCost
{
    public int atkCost;
    public int rangeCost;
    public int fireRateCost;

    public UpgradeCost(int _atkCost, int _rangeCost, int _fireRateCost)
    {
        atkCost = _atkCost;
        rangeCost = _rangeCost;
        fireRateCost = _fireRateCost;

    }
}

public class UpgradeManager : MonoBehaviour
{
    public GameObject upgradingTarget;
    public int numberOfTurret;
    private List<UpgradeCost> upgradeCost = new List<UpgradeCost>();

    public delegate void UpgradeEvent();
    public static event UpgradeEvent OnUpgrade;
    
    private void Awake()
    {
        
        for (int i = 0; i < numberOfTurret; i++)
        {
            upgradeCost.Add(new UpgradeCost(1, 1, 1));
            
        }

    }
    //public delegate void UpgradeHandler();

    //public void UpgradeTurret(UpgradeHandler method)
    //{

    //}
    private void OnEnable()
    {
        ButtonBehaviour.OnClickButton += Upgrade;

    }

    private void OnDisable()
    {
        ButtonBehaviour.OnClickButton -= Upgrade;
    }

    public void Upgrade(UPGRADE_TYPE type)
    {
        switch (type)
        {
            case UPGRADE_TYPE.ATTACK:
                UpgradeAttack(upgradingTarget.GetComponent<MachineGun>().index);
                break;
            case UPGRADE_TYPE.FIRE_RANGE:
                UpgradeRange(upgradingTarget.GetComponent<MachineGun>().index);
                break;
            case UPGRADE_TYPE.FIRE_RATE:
                UpgradeFireRate(upgradingTarget.GetComponent<MachineGun>().index);
                break;
        }
    }

    public void UpgradeAttack(int index)
    {
        if(upgradeCost[index].atkCost > LevelManager.Instance.upgradeMaterial)
        {
            Debug.Log("Not enough");
        }
        else
        {
            upgradingTarget.GetComponent<MachineGun>().damage = upgradingTarget.GetComponent<MachineGun>().damage * 110 / 100;
            LevelManager.Instance.upgradeMaterial -= upgradeCost[index].atkCost;
            var newCost = new UpgradeCost(upgradeCost[index].atkCost + 1, upgradeCost[index].rangeCost, upgradeCost[index].fireRateCost);
            upgradeCost[index] = newCost;
            Debug.Log("attk" + upgradingTarget.GetComponent<MachineGun>().damage);
            OnUpgrade?.Invoke();
        }
        
    }

    public void UpgradeRange(int index)
    {

        if (upgradeCost[index].rangeCost > LevelManager.Instance.upgradeMaterial)
        {
            Debug.Log("Not enough");
        }
        else
        {
            upgradingTarget.GetComponent<MachineGun>().fireRange += 1;
            Debug.Log("range" + upgradingTarget.GetComponent<MachineGun>().fireRange);
            LevelManager.Instance.upgradeMaterial -= upgradeCost[index].rangeCost;
            //cost[index].rangeCost++;
            var newCost = new UpgradeCost(upgradeCost[index].atkCost , upgradeCost[index].rangeCost+1, upgradeCost[index].fireRateCost);
            upgradeCost[index] = newCost;
            OnUpgrade?.Invoke();
        }
        
    }

    public void UpgradeFireRate(int index)
    {
        if (upgradeCost[index].fireRateCost > LevelManager.Instance.upgradeMaterial)
        {
            Debug.Log("Not enough");
        }
        else
        {
            upgradingTarget.GetComponent<MachineGun>().fireRate = upgradingTarget.GetComponent<MachineGun>().fireRate * 95 / 100;
            Debug.Log("rate" + upgradingTarget.GetComponent<MachineGun>().fireRate);
            LevelManager.Instance.upgradeMaterial -= upgradeCost[index].fireRateCost;
            var newCost = new UpgradeCost(upgradeCost[index].atkCost, upgradeCost[index].rangeCost, upgradeCost[index].fireRateCost+1);
            upgradeCost[index] = newCost;
            //cost[index].fireRateCost++;
            OnUpgrade?.Invoke();
        }
        
    }


}
