using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveSkillManager : MonoBehaviour
{
    public GameObject skillOnePrefab; 
    public GameObject skillTwoPrefab;
    public GameObject skillThreePrefab;

    //public Skill skillOneStats;
    //public Skill skillTwoStats;
    //public Skill skillThreeStats;

    public float skillOneCooldown;
    public float skillTwoCooldown;
    public float skillThreeCooldown;

    private float skillOneTimer;
    private float skillTwoTimer;
    private float skillThreeTimer;

    private bool skillOneCanCountdown = false;
    private bool skillTwoCanCountdown = false;
    private bool skillThreeCanCountdown = false;

    //public Slider skillOneSlider;
    //public Slider skillTwoSlider;
    //public Slider skillThreeSlider;

    public SlicedFilledImage skillOneImage;
    public SlicedFilledImage skillTwoImage;
    public SlicedFilledImage skillThreeImage;

    public float percentage = 0;

    private void Start()
    {
        skillOneCooldown = skillOnePrefab.GetComponent<SlowEffect>().skillCooldown;
        skillTwoCooldown = skillTwoPrefab.GetComponent<DistractEffect>().skillCooldown;

        skillOneTimer = 0;
        skillTwoTimer = 0;
        skillThreeTimer = 0;
    }

    private void Update()
    {
        if (skillOneCanCountdown)
        {
            if (skillOneTimer > 0)
            {
                skillOneTimer -= Time.deltaTime;
                skillOneImage.fillAmount = (skillOneTimer / skillOneCooldown);
            }
            else
            {
                skillOneCanCountdown = false;
                //skillOneTimer = skillOneCooldown;
            }
        }
        if (skillTwoCanCountdown)
        {
            if (skillTwoTimer > 0)
            {
                skillTwoTimer -= Time.deltaTime;
                skillTwoImage.fillAmount = (skillTwoTimer / skillTwoCooldown);
            }
            else
            {
                skillTwoCanCountdown = false;
                //skillTwoTimer = skillTwoCooldown;
            }
        }
        if (skillOneCanCountdown)
        {

        }
    }

    public void UsingSkillOne(Vector3 position)
    {
        if(skillOneTimer <= 0 && skillOnePrefab.GetComponent<SlowEffect>().cost <= LevelManager.Instance.upgradeMaterial)
        {
            LevelManager.Instance.upgradeMaterial -= skillOnePrefab.GetComponent<SlowEffect>().cost;
            skillOneCanCountdown = true;
            skillOneTimer = skillOneCooldown;
            Instantiate(LevelManager.Instance.activeSkillManager.skillOnePrefab, position, Quaternion.identity);
        }
        
    }
    public void UsingSkillTwo(Vector3 position)
    {
        if(skillTwoTimer <= 0 && skillTwoPrefab.GetComponent<DistractEffect>().cost <= LevelManager.Instance.upgradeMaterial)
        {
            LevelManager.Instance.upgradeMaterial -= skillTwoPrefab.GetComponent<DistractEffect>().cost;
            skillTwoCanCountdown = true;
            skillTwoTimer = skillTwoCooldown;
            Instantiate(LevelManager.Instance.activeSkillManager.skillTwoPrefab, position, Quaternion.identity);
        }
        
    }
}
