using UnityEngine;

[System.Serializable]
public class Skill : MonoBehaviour
{
    public int cost;
    public float skillCooldown;
    //public float skillTimer;
    public float effectTime;
    //public GameObject skillPrefab;

}
