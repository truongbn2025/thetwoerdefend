
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
public class SlowEffect : Skill
{

    private void Start()
    {
        StartCoroutine(AutoDestroy());
    }

    IEnumerator AutoDestroy()
    {
        yield return new WaitForSeconds(effectTime);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.transform.CompareTag("Enemy"))
        {
            other.GetComponentInParent<NavMeshAgent>().speed = 0.5f;
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Enemy"))
        {
            other.GetComponentInParent<NavMeshAgent>().speed = 2;
            
        }
    }
}
