using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SwitchCharacter : MonoBehaviour
{
    public MyInputAction myInputAction;
    public InputAction changeCharacter;
    public List<GameObject> characterMech;
    public GameObject virtualCameraFollowCharacterOne;
    public GameObject virtualCameraFollowCharacterTwo;
    private int characterIndex;
    private void Awake()
    {
        myInputAction = new MyInputAction();
        virtualCameraFollowCharacterOne.SetActive(true);
        virtualCameraFollowCharacterTwo.SetActive(false);
    }

    private void OnEnable()
    {
        changeCharacter = myInputAction.LevelManager.ChangeCharacter;
        changeCharacter.performed += SwitchMyCharacter;
        myInputAction.LevelManager.ChangeCharacter.Enable();



    }
    private void Start()
    {
        SwitchToCharacterIndex(0);
    }
    private void OnDisable()
    {
        changeCharacter.performed -= SwitchMyCharacter;
        changeCharacter.Disable();
        
    }


    


    public void SwitchMyCharacter(InputAction.CallbackContext context)
    {
        if (characterIndex < characterMech.Count-1)
        {
            characterIndex++;
        }
        else
        {
            characterIndex = 0;
        }
        SwitchToCharacterIndex(characterIndex);
    }

    void SwitchToCharacterIndex(int index)
    {
        //virtualCamera.m_Follow = characterMech[index].transform;
        if(index == 0)
        {
            virtualCameraFollowCharacterOne.SetActive(true);
            virtualCameraFollowCharacterTwo.SetActive(false);
        }
        if (index == 1)
        {
            virtualCameraFollowCharacterOne.SetActive(false);
            virtualCameraFollowCharacterTwo.SetActive(true);
        }
        characterMech[index].GetComponent<PlayerControllerTDG>().enabled = true;
        //Debug.Log(index);
        for (int i = 0; i < characterMech.Count; i++)
        {
            if (i != index)
                characterMech[i].GetComponent<PlayerControllerTDG>().enabled = false;
        }
            
        //characterMech[index].SetActive(true);
    }
}
